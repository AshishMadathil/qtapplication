#ifndef RENDERAREA_H
#define RENDERAREA_H

#include <QWidget>
#include <QColor>

class RenderArea : public QWidget
{
    Q_OBJECT
public:
    explicit RenderArea(QWidget *parent = nullptr);

    QSize minimumSizeHint() const Q_DECL_OVERRIDE;
    QSize sizeHint() const Q_DECL_OVERRIDE;
    enum ShapeType {Astroid, Cycloid, HuygensCycloid, HypoCycloid};

    void setBackgroundColor(QColor color){mBackgroundColor = color;} //setter (mBackgroundColor is declared below as a private attribute)
    QColor backgroundColor()const {return mBackgroundColor;} //getter
    // 'const' qualifier added so that the function doesn't modify any member class variable
    void setShape (ShapeType shape){mShape = shape;}  //setter  (mShape is declared below as a private variable as part of enum datatype)
    ShapeType shape() const {return mShape;}     //getter

/*
     void setShape (ShapeType shape)          /* setter */
  /* {
         mShape = shape;
     }

     ShapeType shape() const                  /* getter */
  /* {
         return mShape;
     }

  *****************************************************************

     void setBackgroundColor(QColor color)    /* setter */
  /* {
         mBackgroundColor = color;
     }

     QColor backgroundColor() const           /* getter */
  /* {
         return mBackgroundColor;
     }

*/


protected:
    void paintEvent(QPaintEvent *event) Q_DECL_OVERRIDE;

signals:

public slots:

private:
    QColor mBackgroundColor;
    QColor mShapeColor;
    ShapeType mShape;
};

#endif // RENDERAREA_H
